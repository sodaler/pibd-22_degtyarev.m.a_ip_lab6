package ru.ulstu.is.sbapp.worker.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.ulstu.is.sbapp.worker.service.CityService;
import ru.ulstu.is.sbapp.worker.service.WorkerService;

import javax.validation.Valid;

@Controller
@RequestMapping("/city")
public class CirtyMvcController {
    private final CityService cityService;
    private final WorkerService workerService;

    public CirtyMvcController(CityService cityService, WorkerService workerService) {
        this.cityService = cityService;
        this.workerService = workerService;
    }

    @GetMapping
    public String getCities(Model model) {
        model.addAttribute("cities",
                cityService.findAllCities().stream()
                        .map(CityDto::new)
                        .toList());
        return "city";
    }

    @GetMapping(value = {"/edit", "/edit/{id}"})
    public String editCity(@PathVariable(required = false) Long id,
                           Model model) {
        if (id == null || id <= 0) {
            model.addAttribute("cityDto", new CityDto());
        } else {
            model.addAttribute("cityId", id);
            model.addAttribute("cityDto", new CityDto(cityService.findCity(id)));
            model.addAttribute("workers",
                    workerService.findAllWorkers().stream()
                            .map(WorkerDto::new)
                            .toList());
        }
        return "city-edit";
    }

    @PostMapping(value = {"", "/{id}"})
    public String saveCity(@PathVariable(required = false) Long id,
                           @ModelAttribute @Valid CityDto cityDto,
                           BindingResult bindingResult,
                           Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("errors", bindingResult.getAllErrors());
            return "city-edit";
        }
        if (id == null || id <= 0) {
            cityService.addCity(cityDto.getName());
        } else {
            cityService.updateCity(cityDto);
        }
        return "redirect:/city";
    }

    @PostMapping("/delete/{id}")
    public String deleteCity(@PathVariable Long id) {
        cityService.deleteCity(id);
        return "redirect:/city";
    }
}
