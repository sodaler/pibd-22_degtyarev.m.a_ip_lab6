package ru.ulstu.is.sbapp.worker.controller;

import org.springframework.web.bind.annotation.*;
import ru.ulstu.is.sbapp.configuration.WebConfiguration;
import ru.ulstu.is.sbapp.worker.service.CityService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(WebConfiguration.REST_API + "/city")
public class CityController {
    private final CityService cityService;

    public CityController(CityService cityService) {
        this.cityService = cityService;
    }

    @GetMapping("/{id}")
    public CityDto getCity(@PathVariable Long id) {
        return new CityDto(cityService.findCity(id));
    }

    @GetMapping("/")
    public List<CityDto> getCities() {
        return cityService.findAllCities().stream()
                .map(CityDto::new)
                .toList();
    }

    @PostMapping("/")
    public CityDto createCity(@RequestParam("Name") String name) {
        return new CityDto(cityService.addCity(name));
    }

    @PatchMapping("/")
    public CityDto updateCity(@RequestBody @Valid CityDto cityDto) {
        return cityService.updateCity(cityDto);
    }
    @DeleteMapping("/{id}")
    public CityDto deleteCity(@PathVariable Long id) {
        return new CityDto(cityService.deleteCity(id));
    }
    
}

