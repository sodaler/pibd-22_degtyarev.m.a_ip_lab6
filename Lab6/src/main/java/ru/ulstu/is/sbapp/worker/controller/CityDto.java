package ru.ulstu.is.sbapp.worker.controller;

import ru.ulstu.is.sbapp.worker.model.City;

import javax.validation.constraints.NotBlank;

public class CityDto {
    private long id;
    @NotBlank
    private String name;
    private String workers ="";

    public CityDto(){}

    public CityDto(City city) {
        this.id = city.getId();
        this.name = city.getName();
        if(city.getWorkers()!=null)
            this.workers = city.getWorkers().toString();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getWorkers() {
        return workers;
    }

    public void setName(String name) {
        this.name = name;
    }
}

