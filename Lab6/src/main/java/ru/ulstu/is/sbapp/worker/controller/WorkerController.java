package ru.ulstu.is.sbapp.worker.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.ulstu.is.sbapp.configuration.WebConfiguration;
import ru.ulstu.is.sbapp.worker.service.WorkerService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(WebConfiguration.REST_API + "/worker")
public class WorkerController {
    private final WorkerService workerService;

    public WorkerController(WorkerService workerService) {
        this.workerService = workerService;
    }

    @GetMapping("/{id}")
    public WorkerDto getWorker(@PathVariable Long id) {
        return new WorkerDto(workerService.findWorker(id));
    }

    @GetMapping
    public List<WorkerDto> getWorkers() {
        return workerService.findAllWorkers().stream()
                .map(WorkerDto::new)
                .toList();
    }

    @PostMapping("/")
    public WorkerDto createWorker(@RequestBody @Valid WorkerDto workerDto) {
        return workerService.addWorker(workerDto);
    }

    @PutMapping("/{id}")
    public WorkerDto updateWorker(@RequestBody @Valid WorkerDto workerDto) {
        return workerService.updateWorker(workerDto);
    }


    @DeleteMapping("/{id}")
    public WorkerDto deleteWorker(@PathVariable Long id) {
        return new WorkerDto(workerService.deleteWorker(id));
    }
}