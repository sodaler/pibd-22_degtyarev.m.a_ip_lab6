package ru.ulstu.is.sbapp.worker.controller;

import ru.ulstu.is.sbapp.worker.model.Worker;

import javax.validation.constraints.NotBlank;

public class WorkerDto {
    private long id;
    @NotBlank
    private String firstName;
    @NotBlank
    private String lastName;
    private String serial;
    private String number;
    private String city;

    public WorkerDto(Worker worker) {
        this.id = worker.getId();
        this.firstName= worker.getFirstName();
        this.lastName= worker.getLastName();
        this.serial = worker.getPassport().getSerial();
        this.number= worker.getPassport().getNumber();
        this.city= worker.getCity().getName();
    }

    public WorkerDto() {
    }

    public long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getSerial() {
        return serial;
    }

    public String getNumber() {
        return number;
    }

    public String getCity() {
        return city;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setId(long id) {
        this.id = id;
    }
}
