package ru.ulstu.is.sbapp.worker.model;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class City {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    @OneToMany(mappedBy = "city", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Worker> workers;

    public City(){ }

    public City(String name){
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        City city = (City) o;
        return Objects.equals(id, city.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Worker> getWorkers() {
        return workers;
    }

    public void addWorker(Worker worker) {
        workers.add(worker);
    }
    public void updateWorker(Long workerID, Worker wk) {
        for(Worker worker : workers){
            if(worker.getId()==wk.getId()) {
                worker = wk;
                break;
            }
        }
    }
    public void removeStudent(Long workerID){
        workers.remove(workerID);
    }
    public void removeAllStudents(){
        workers.clear();
    }
}

