package ru.ulstu.is.sbapp.worker.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Passport {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String serial;
    private String number;

    public Passport(){ }

    public Passport(String serial, String number){
        this.number = number;
        this.serial = serial;
    }

    public String getSerial() {
        return serial;
    }

    public String getNumber() {
        return number;
    }

    public Long getId() {
        return id;
    }
    @Override
    public String toString() {
        return "Passport{" +
                "id=" + id +
                ", serial='" + serial + '\'' +
                ", number='" + number + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Passport passport = (Passport) o;
        return Objects.equals(id, passport.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}