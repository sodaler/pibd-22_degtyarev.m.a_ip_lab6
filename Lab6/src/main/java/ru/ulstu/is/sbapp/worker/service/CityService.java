package ru.ulstu.is.sbapp.worker.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import ru.ulstu.is.sbapp.worker.controller.CityDto;
import ru.ulstu.is.sbapp.worker.model.City;
import ru.ulstu.is.sbapp.worker.repository.CityRepository;
import ru.ulstu.is.sbapp.util.validation.ValidatorUtil;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CityService {
    private final Logger log = LoggerFactory.getLogger(CityService.class);
    private final CityRepository cityRepository;
    private final ValidatorUtil validatorUtil;

    public CityService(CityRepository cityRepository,
                       ValidatorUtil validatorUtil) {
        this.cityRepository = cityRepository;
        this.validatorUtil = validatorUtil;
    }

    @Transactional
    public City addCity(String name) {
        if (!StringUtils.hasText(name)) {
            throw new IllegalArgumentException("City data is null or empty");
        }
        final City city = new City(name);
        validatorUtil.validate(city);
        return cityRepository.save(city);
    }

    @Transactional(readOnly = true)
    public City findCity(Long id) {
        final Optional<City> city = cityRepository.findById(id);
        return city.orElseThrow(() -> new CityNotFoundException(id));
    }

    //this
    @Transactional(readOnly = true)
    public City findCityByName(String name) {
        List<City> cities = cityRepository.findAll();
        for (City city : cities) {
            if (city.getName().equals(name))
                return city;
        }
        throw new EntityNotFoundException(String.format("City with name [%s] is not found", name));
    }

    @Transactional(readOnly = true)
    public List<City> findAllCities() {
        return cityRepository.findAll();
    }

    @Transactional
    public City updateCity(Long id, String name) {
        if (!StringUtils.hasText(name)) {
            throw new IllegalArgumentException("City name is null or empty");
        }
        final City currentCity = findCity(id);
        currentCity.setName(name);
        validatorUtil.validate(currentCity);
        return cityRepository.save(currentCity);
    }

    public CityDto updateCity(CityDto cityDto) {
        return new CityDto(updateCity(cityDto.getId(), cityDto.getName()));
    }

    @Transactional
    public City deleteCity(Long id) {
        City currentCity = findCity(id);
        cityRepository.delete(currentCity);
        return currentCity;
    }

    @Transactional
    public void deleteAllCitiesUnsafe() {
        log.warn("Unsafe usage!");
        List<City> cities = findAllCities();
        for(City city : cities){
            if(city.getWorkers().size()>0)
                city.removeAllStudents();
        }
        cityRepository.deleteAll();
    }

    @Transactional
    public void deleteAllWorkers() throws InCityFoundWorkersException {
        List<City> groups = findAllCities();
        for(City city : groups){
            if(city.getWorkers().size()>0)
                throw new InCityFoundWorkersException("в городе" + city.getName() + "есть рабочие");
        }
        cityRepository.deleteAll();
    }
    
}
