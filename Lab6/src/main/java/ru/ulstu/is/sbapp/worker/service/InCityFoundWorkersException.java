package ru.ulstu.is.sbapp.worker.service;

public class InCityFoundWorkersException extends Exception {
    public InCityFoundWorkersException(String errorMessage) {
        super(errorMessage);
    }
}
